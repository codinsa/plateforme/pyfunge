[![Licence GPL][licence-badge]][licence]
[![Coverage report][coverage-badge]][coverage]
[![Pipeline status][pipeline-badge]][pipeline]

[licence-badge]: https://img.shields.io/badge/License-MIT-green.svg
[licence]: LICENSE

[pipeline-badge]: https://gitlab.com/codinsa/plateforme/pyfunge/badges/default/pipeline.svg
[pipeline]: https://gitlab.com/codinsa/plateforme/pyfunge/commits/default

[coverage-badge]: https://gitlab.com/codinsa/plateforme/pyfunge/badges/default/coverage.svg
[coverage]: https://gitlab.com/codinsa/plateforme/pyfunge/commits/default

# PyFunge

This repository is a fork of PyFunge mercurial repository. The fork is based on:
- A copy of the mercurial files available on [Mercurial Bitbucket archive](https://bitbucket-archive.softwareheritage.org/projects/li/lifthrasiir/pyfunge.html)
- A copy of the Bitbucket repository files on [Wayback Machine archive](https://web.archive.org/web/20200622104219if_/https://bitbucket.org/lifthrasiir/pyfunge/get/8350253d44a1.zip)
- The content of the [PyPi project](https://pypi.org/project/PyFunge/) and [original project infos](https://mearie.org/projects/pyfunge/)

> Disclaimer: There may be missing commits since this fork doesn't have any guarantee to be up-to-date with the currently lost mercurial repository.

**{- Please, note that this repository will probably never be published on PyPi. -}**

## Mercurial to Git migration

If needed, this repository contains also some git notes to map the mercurial and the git commit revisions.
You can fetch them by using `git fetch origin refs/notes/*:refs/notes/*`:
- To see the mapped correspondance, you can use `git log --notes=hg <commit_revision>`
- To see the correspondance between a cited mercurial commit revision in a commit message, and its new git commit revision, you can use `git log --notes=commits <commit_revision>`
- To see both mercurial and git correspondance, you can just use `git log --notes=hg --notes=commits <commit_revision>`

## Dependency information

The project uses Python 2.7 to run, and [Mycology](https://deewiant.iki.fi/projects/mycology/) (see its [GitHub repository](https://github.com/Deewiant/Mycology)) to run test.

## Changes

The changelog can be found within the [documentation](docs/changes.rst) and in the [CHANGES](CHANGES.md) file.

## License

PyFunge is licensed under the terms of the [MIT license](LICENSE).
