# Cod'INSA PyFunge change-log

The latest version of this file can be found at the default branch of the Cod'INSA PyFunge repository.

## 0.5

- More tests in `tests/` directory.
- Added proper license file and templates for GitLab MR/issue.
- Added a README file, a CHANGES file (outside the documentation) and a contribution guide for the repository.
- Moved to [Git](https://git-scm.com/) repository.

## 0.5-rc2

- Bug fixes.
  - Befunge-93 space now treats the cell as 8-bit unsigned integer.
  - New IPs cloned by `t` command are now guaranteed to be executed before parent IP.
  - `u` command does obey queuemode and invertmode from `MODE`.
  - `F` command of `TIME` was off by one. (0 should mean the first day of year.)
- Internal API changes. These changes don’t affect `PyFunge v2` fingerprint API, but does require some trivial changes.
  - The return value of command callback (previously used to be walk flag) is now unused. Fingerprint modules for 0.5-rc1 will work correctly, but **I** strongly recommends to return `None` (or don’t use `return` at all) always.
  - `funge.semantics.Semantics.execute()` is gone, due to the prior change.
  - `funge.semantics.Semantics.walk()` is now a part of public API.
  - Individual access to `funge.stack.Stack()` is strongly discouraged. You should use `funge.ip.IP()`‘s stack stack wrapper instead. (If your fingerprint works on the stack stack, your code can be affected.)
  - `funge.ip.IP.th_id` and `th_team` became `funge.ip.IP.id` and `team`. `funge.ip.IP.parentid` is also available now.
  - `funge.space.Space.put()` has an optional third argument to optimize bulk changes to Funge space. You can use `funge.space.Space.notifyrect()` with it.
  - `funge.space.Space.rectmin` and `rectmax` became `funge.space.Space.boundmin` and `boundmax`. If you want the exact bounds use `funge.space.Space.getrect()` instead.
- Extensive optimizations resulted in 10–30% overall speed-up.
  - `funge.space.Space.putspace()` is 3x faster for huge code (say, more than 10000 commands).
  - `funge.space.Space.scanwhile()` and `scanuntil` is 2x faster for general case. These methods were bottlenecks of entire Funge code execution.
  - The stack API is revised to perform well even with or without invertmode and queuemode.
  - `funge.program.Program.execute()` now special-cases single IP, and 2x faster for that case.
- New Funge-98 fingerprints: `3DSP`, `ICAL`, `IIPC`, `IMAP`, `IMTH`, `LONG`, `NCRS`, `PERL`, `SCKE`, `SETS`, `SOCK`, `SUBR` and `TERM`.
- Provides the complete documentation. Earlier version has some missing sections yet to be written.
- More regression tests in `tests/` directory.

## 0.5-rc1

- Full Befunge-98 compliance.
  - Rewrote problematic IP walking algorithm to get correct behavior of `#`, `'`, `s` and concurrency.
  - Fixed `[` and `]` command which got swapped.
  - Fixed `/` and `%` command so division by zero results in zero, not reflects.
  - Corrected `k` command. (Meaning of it was quite dubious, but now clear)
  - Fixed `{`, `}` and `u` command which had a number of bugs, including treating the argument with incorrect sign.
  - `:` always pushes two items, even if the stack has one or zero items.
  - Fixed faulty treatment of Unefunge and Befunge source code.
  - Almost complete change in internal API.
- A lot of code restructuring and refactoring.
  - All packages are merged into funge package.
  - Renamed internal classes.
  - `funge.execute` module splits into `funge.languages` package.
  - Introduced `funge.vector.Vector` class for convenience.
- Big improvement in performance (10x-40x speed gain), including:
  - More efficient space-related algorithms; they are also used only when needed. (8x speed gain)
  - Added dimension-dependent, caching Vector class. (2x speed gain)
  - General code simplification. (1.5x-2x speed gain)
- New command-line front-end, allowing the direct execution of all available languages.
- New fingerprint implementation, which API dubbed “PyFunge v2”. (See Writing fingerprint for more information)
- Implemented more fingerprints. (See Supported fingerprints for more information)
- Added the regression test suite. (See Tests for more information)
- Moved to [Mercurial](https://www.selenic.com/mercurial/) repository.

## 0.2-beta1

- Initial release.